package com.example.applocker.ListAdapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.applocker.DataBase;
import com.example.applocker.Model.AppName;
import com.example.applocker.R;

import java.util.ArrayList;

import static com.example.applocker.DataBase.FALSE;
import static com.example.applocker.DataBase.TRUE;
import static com.example.applocker.Model.AppName.list;

/**
 * Created by Антон on 26.02.2017.
 */

public class AppNameAdapter extends ArrayAdapter<AppName> {

        Context context;
        ArrayList<AppName> appName;
        DataBase dataBase;

    public AppNameAdapter(Context context, int resource, ArrayList<AppName> appName) {
        super(context, resource, appName);
        this.context = context;
        this.appName = appName;
        dataBase = new DataBase(context);
        }

    static class ViewHolder {
        ImageView imageView;
        TextView textView;
        Switch switch1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.app_name_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            viewHolder.textView = (TextView) convertView.findViewById(R.id.textView);
            viewHolder.switch1 = (Switch) convertView.findViewById(R.id.switch1);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.imageView.setImageDrawable(appName.get(position).getIcon());
        viewHolder.textView.setText(appName.get(position).getText());
//        viewHolder.textView.setText(appName.get(position).getPackageName());
        viewHolder.switch1.setChecked(appName.get(position).getAuth());

        viewHolder.switch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewHolder.switch1.isChecked()){
                    list.get(position).setAuth(true);
                    appName.get(position).setAuth(true);
                    dataBase.updateApp(appName.get(position).getPackageName(), TRUE);
                }else{
                    list.get(position).setAuth(false);
                    appName.get(position).setAuth(false);
                    dataBase.updateApp(appName.get(position).getPackageName(), FALSE);
                }
            }
        });

//        viewHolder.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                System.out.println(b);
//                if(b){
//                    list.get(position).setAuth(true);
//                    appName.get(position).setAuth(true);
//                    dataBase.updateApp(appName.get(position).getPackageName(), TRUE);
//                    System.out.println(appName.get(position).getAuth());
//                }else{
//                    list.get(position).setAuth(false);
//                    appName.get(position).setAuth(false);
//                    dataBase.updateApp(appName.get(position).getPackageName(), FALSE);
//                }
//            }
//        });
        return convertView;
    }


}
