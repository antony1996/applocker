package com.example.applocker.Model;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;

/**
 * Created by Антон on 26.02.2017.
 */
public class AppName {

    public static ArrayList<AppName> list = new ArrayList<>();

    private Drawable icon;
    private String text;
    private String packageName;
    private boolean auth;

    public AppName(Drawable icon, String text, String packageName,  boolean auth) {
        this.icon = icon;
        this.text = text;
        this.auth = auth;
        this.packageName = packageName;
    }
    public AppName() {}

    public Drawable getIcon() {
        return icon;
    }

    public String getText() {
        return text;
    }

    public String getPackageName() {
        return packageName;
    }

    public boolean getAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }
}
