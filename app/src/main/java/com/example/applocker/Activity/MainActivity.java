package com.example.applocker.Activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.applocker.DataBase;
import com.example.applocker.ListAdapter.AppNameAdapter;
import com.example.applocker.Model.AppName;
import com.example.applocker.R;
import com.example.applocker.Service.MyService;

import java.util.List;

import static com.example.applocker.DataBase.FALSE;
import static com.example.applocker.DataBase.NULL;
import static com.example.applocker.DataBase.TRUE;
import static com.example.applocker.Model.AppName.list;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayAdapter <AppName> arrayAdapter;
    DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBase =  new DataBase(getApplicationContext());
        dataBase.openOfCreateDatabase();

        if (!isAccessibilitySettingsOn(getApplicationContext()))
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));

        listView = (ListView) findViewById(R.id.list_view);
        arrayAdapter = new AppNameAdapter(this, R.layout.app_name_item, list);
        listView.setAdapter(arrayAdapter);

        getAllApps();
    }

    private void getAllApps(){
        if(list.size()!= 0)
            list.clear();

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pkgAppsList = this.getPackageManager().queryIntentActivities( mainIntent, 0);
        for (ResolveInfo rf : pkgAppsList){

            switch (dataBase.getAppAuth(rf.activityInfo.packageName)){
                case TRUE:
                    list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, true));
                    break;

                 case FALSE:
                     list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, false));
                    break;

                 case NULL:
                     list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, false));
                     dataBase.insertApp(rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, FALSE);
                    break;
            }
        }
        arrayAdapter.notifyDataSetChanged();
    }

    private boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = getPackageName() + "/" + MyService.class.getCanonicalName();

        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ACCESSIBILITY_ENABLED);

        } catch (Settings.SettingNotFoundException ignored) {}
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();

                    if (accessibilityService.equalsIgnoreCase(service))
                        return true;

                }
            }
        }
        return false;
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}
