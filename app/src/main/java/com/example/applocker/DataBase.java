package com.example.applocker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Антон on 27.02.2017.
 */

public class DataBase {
    private final String DATA_BASE_NAME = "InstallApps.db";
    private final String APP_TABLE = "Apps";
    private final String APP_NAME_COLUMN = "app_name";
    private final String APP_PACKAGE_NAME_COLUMN = "app_package_name";
    private final String APP_AUTH_COLUMN = "app_auth";

    public static final int TRUE = 1;
    public static final int FALSE = 0;
    public static final int NULL = -1;

    private Context context;

    public DataBase(Context context){
        this.context = context;
    }

    public void openOfCreateDatabase() {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS " + APP_TABLE +
                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                APP_NAME_COLUMN + " STRING," +
                APP_PACKAGE_NAME_COLUMN + " STRING," +
                APP_AUTH_COLUMN + " INTEGER" +
                ");");
        database.close();
    }

    public void insertApp (String appName, String packageName, int auth) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues values = new ContentValues();
        values.put(APP_NAME_COLUMN, appName);
        values.put(APP_PACKAGE_NAME_COLUMN, packageName);
        values.put(APP_AUTH_COLUMN, auth);
        database.insert(APP_TABLE, null, values);
        database.close();
    }

    public void updateApp(String packageName, int auth){
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues updateAuth = new ContentValues();
        updateAuth.put(APP_AUTH_COLUMN, auth);
        database.update(APP_TABLE, updateAuth, APP_PACKAGE_NAME_COLUMN + " = ?", new String[]{packageName});
        database.close();
    }


    public int getAppAuth(String packageName) {
        int auth = NULL;
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(APP_TABLE, null, APP_PACKAGE_NAME_COLUMN + " = ?", new String[]{packageName}, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            if(cursor.getInt(3) == TRUE)
                auth = TRUE;
            else if (cursor.getInt(3) == FALSE)
                auth = FALSE;
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return auth;
    }
}
