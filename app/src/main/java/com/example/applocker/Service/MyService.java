package com.example.applocker.Service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.example.applocker.Activity.LockActivity;
import com.example.applocker.DataBase;
import com.example.applocker.Model.AppName;

import java.util.List;

import static com.example.applocker.DataBase.FALSE;
import static com.example.applocker.DataBase.NULL;
import static com.example.applocker.DataBase.TRUE;
import static com.example.applocker.Model.AppName.list;


public class MyService extends AccessibilityService {

    public static boolean auth = false;

    DataBase dataBase;

        @Override
        protected void onServiceConnected() {
            super.onServiceConnected();

            dataBase = new DataBase(getApplicationContext());
            dataBase.openOfCreateDatabase();

            if(list.size() == 0){
                Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
                mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                List<ResolveInfo> pkgAppsList = this.getPackageManager().queryIntentActivities( mainIntent, 0);
                for (ResolveInfo rf : pkgAppsList){

                    switch (dataBase.getAppAuth(rf.activityInfo.packageName)){
                        case TRUE:
                            list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, true));
                            break;

                        case FALSE:
                            list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, false));
                            break;

                        case NULL:
                            list.add(new AppName(rf.activityInfo.loadIcon(getPackageManager()), rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, false));
                            dataBase.insertApp(rf.activityInfo.loadLabel(getPackageManager()).toString(),rf.activityInfo.packageName, FALSE);
                            break;
                    }
                }
            }

            AccessibilityServiceInfo config = new AccessibilityServiceInfo();
            config.eventTypes = AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED;
            config.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
            config.flags = AccessibilityServiceInfo.FLAG_INCLUDE_NOT_IMPORTANT_VIEWS;

            setServiceInfo(config);
        }

        @Override
        public void onAccessibilityEvent(AccessibilityEvent event) {
            if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
                if (event.getPackageName() != null && event.getClassName() != null) {
                    ComponentName componentName = new ComponentName(
                            event.getPackageName().toString(),
                            event.getClassName().toString()
                    );

                    ActivityInfo activityInfo = tryGetActivity(componentName);
                    boolean isActivity = activityInfo != null;

                    Log.i("CurrentActivity", componentName.flattenToShortString());

                    if(isActivity && componentName.flattenToShortString().contains("launcher")){
                        auth = false;
                    }

                    for(int i = 0; i < list.size(); i++){
                        if(isActivity && list.get(i).getAuth() && componentName.flattenToShortString().contains(list.get(i).getPackageName())
                                && !auth){
                            Log.i("LockActivity", componentName.flattenToShortString());
                            Intent intent = new Intent(MyService.this, LockActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    }
                }
            }
        }


        private ActivityInfo tryGetActivity(ComponentName componentName) {
            try {
                return getPackageManager().getActivityInfo(componentName, 0);
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        }

        @Override
        public void onInterrupt() {}
}
