# Application Locker #

Приложение предназначено для показа специального экрана перед открытием приложений. Список приложений можно найти на главном экране и выбрать в каком приложении использовать данную функцию.

### В приложении использовались: ###

* ListView
* AccessibilityService
* SQLite 

### Список приложений ###

![Screenshot_1490046935.png](https://bitbucket.org/repo/5qxAXjq/images/500757464-Screenshot_1490046935.png)

### Окно для входа ###

![Screenshot_1490046950.png](https://bitbucket.org/repo/5qxAXjq/images/94259640-Screenshot_1490046950.png)

### После нажатия на кнопку "Войти" ###

![Screenshot_1490046952.png](https://bitbucket.org/repo/5qxAXjq/images/1303363506-Screenshot_1490046952.png)